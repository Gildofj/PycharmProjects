class calcula:

    listanum = []
    soma = 0


    def insere(self):
        for c in range(10):
            self.listanum.append(int(input(f'Insira o valor do item {c}: ')))
            self.soma = self.soma + self.listanum[c]
        print("*/" * 30)
        print(f'Voce digitou os valores {self.listanum}')
        self.Maior_Menor()
        self.calculo()


    def Maior_Menor(self):
        maior = 0
        menor = 0
        for c in self.listanum:
            if self.listanum[c] > maior:
                maior = self.listanum[c]
            if self.listanum[c] < menor:
                menor = self.listanum[c]

        print(f'O maior valor foi o {maior} na(s) posição(ões): ', end='')
        for i, v in enumerate(self.listanum):
            if v == maior:
                print(f'{i}.', end='')
        print()

        print(f'O menor valor foi o {menor} na(s) posição(ões): ', end='')
        for i, v in enumerate(self.listanum):
            if v == menor:
                print(f'{i}.', end='')
        print()

    def calculo(self):
        media = self.soma / len(self.listanum)
        print(f'A soma de todos os valores é {self.soma} e a media aritmética é {media}')

