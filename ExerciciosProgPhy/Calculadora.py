def main():
    a = int(input("Digite o primeiro valor a ser calculado: "))
    b = int(input("Digite o segundo valor a ser calculado: "))

    soma = a + b
    subt = a - b
    multi = a * b
    divi = a / b

    print("Operacoes de", a,"e", b,":")
    print("Soma: ", a,"+",b,"=", soma)
    print("Subtracao: ", a,"-", b,"=", subt)
    print("Multiplicao: ", a,"*", b,"=", multi)
    print("Divisao: ", a,"/", b,"=", divi)


main()
