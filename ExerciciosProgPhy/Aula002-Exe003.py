from calculadorac import Calculadora

valor1 = int(input("Informe o valor 1"))
valor2 = int(input("Informe o valor 2"))

calculadorac = Calculadora()
result1 = calculadorac.soma(valor1,valor2)
result2 = calculadorac.subtrai(valor1,valor2)
result3 = calculadorac.divide(valor1,valor2)
result4 = calculadorac.multiplica(valor1,valor2)

print("A soma é ", result1 + 1)
print("A subtração é ", result2 + 1)
print("A divisão é ", result3 + 1)
print("A multiplicação é ", result4 + 1)