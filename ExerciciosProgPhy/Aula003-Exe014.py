print('TABUADA DO OITO')
print('=-' * 8)
n = 8
x = 1
while x <= 10:
    for c in range(10):
        i = c+1
        print(f'8x{i}={n*x}')
        x = x + 1
print('-/'* 30)