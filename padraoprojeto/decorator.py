import datetime

def calcula_tempo_decorator(funcao):
    def calcula_tempo():
        print(datetime.datetime.now())
        funcao()
        print(datetime.datetime.now())
    return calcula_tempo