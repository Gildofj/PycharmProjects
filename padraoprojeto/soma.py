def cria_soma(x):

    def soma(num):
        return x + num
    return soma

soma2 = cria_soma(2)
soma3 = cria_soma(3)

print(soma2(2))
print(soma3(2))
