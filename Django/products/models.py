from django.db import models


class Product(models.Model):
    description = models.CharField(max_legth=100)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    quantity = models.IntegerFiel()

    def __str__(self):
        return self.description
