import time

def calcula_duracao(funcao):
    def calcula_tempo():
        tempo_inicial = time.time()
        funcao()
        tempo_final = time.time()


        print("Tempo total de execução: {tempo_total}".format(tempo_total=str(tempo_final - tempo_inicial)))
    return calcula_tempo